package net.sf.okapi.connectors.google;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;

public interface GoogleMTAPI {
    List<TranslationResponse> translate(String srcCode, String tgtCode, List<String> sourceTexts)
                throws IOException, ParseException;
    List<String> getLanguages() throws IOException, ParseException;
}
