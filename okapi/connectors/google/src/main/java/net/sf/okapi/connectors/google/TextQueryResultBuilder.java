package net.sf.okapi.connectors.google;

import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.query.QueryResult;
import net.sf.okapi.common.resource.TextFragment;

class TextQueryResultBuilder extends QueryResultBuilder<String> {
    TextQueryResultBuilder(GoogleMTv2Parameters params, String name, int weight) {
        super(params, name, weight);
    }

    @Override
    List<QueryResult> convertResponses(List<TranslationResponse> responses, String text) {
        List<QueryResult> results = new ArrayList<>();
        for (TranslationResponse response : responses) {
            QueryResult qr = createQueryResult(response);
            qr.source = (response.getSource() == null) ? new TextFragment(text) : new TextFragment(response.getSource());
            qr.target = new TextFragment(response.getTarget());
            results.add(qr);
        }
        return results;
    }
}