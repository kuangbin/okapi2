package net.sf.okapi.connectors.google;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GoogleResponseParser {
    private JSONParser parser = new JSONParser();

    public List<String> parseResponse(Reader r) throws IOException, ParseException {
        return parseArrayResponse(r, "translations", "translatedText");
    }

    public List<String> parseLanguagesResponse(Reader r) throws IOException, ParseException {
        return parseArrayResponse(r, "languages", "language");
    }

    protected List<String> parseArrayResponse(Reader r, String arrayName, String arrayKey)
                                throws IOException, ParseException {
        JSONObject json = (JSONObject)parser.parse(r);
        JSONArray array = requireArray(require(json, "data"), arrayName);
        List<String> values = new ArrayList<>();
        for (Object o : array) {
            if (o instanceof JSONObject) {
                values.add(unescapeTranslation(requireString((JSONObject)o, arrayKey)));
            }
        }
        return values;
    }

    // There is more we could parse here, but for now just parse the overall error message. We
    // can log the full JSON if needed.
    public String parseError(Reader r) throws IOException, ParseException {
        JSONObject json = (JSONObject)parser.parse(r);
        return requireString(require(json, "error"), "message");
    }

    // Google seems to assume the content type is HTML, and returns &, <, >, ", using named entities,
    // and ' as &#39;.  Other characters are returned using JSON's normal unicode escape mechanism.
    private String unescapeTranslation(String text) {
        text = text.replace("&#39;", "'");
        text = text.replace("&lt;", "<");
        text = text.replace("&gt;", ">");
        text = text.replace("&quot;", "\"");
        return text.replace("&amp;", "&");
    }

    private JSONObject require(JSONObject json, String key) {
        Object o = json.get(key);
        if (o == null || !(o instanceof JSONObject)) {
            throw new IllegalArgumentException("JSON didn't contain expected object " + key);
        }
        return (JSONObject)o;
    }
    private String requireString(JSONObject json, String key) {
        Object o = json.get(key);
        if (o == null || !(o instanceof String)) {
            throw new IllegalArgumentException("JSON didn't contain expected object " + key);
        }
        return (String)o;
    }
    private JSONArray requireArray(JSONObject json, String key) {
        Object o = json.get(key);
        if (o == null || !(o instanceof JSONArray)) {
            throw new IllegalArgumentException("JSON didn't contain expected array " + key);
        }
        return (JSONArray)o;
    }
}
