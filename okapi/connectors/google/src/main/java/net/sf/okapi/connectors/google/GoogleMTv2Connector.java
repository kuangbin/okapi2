/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.connectors.google;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.query.QueryResult;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.lib.translation.BaseConnector;
import net.sf.okapi.lib.translation.QueryUtil;

public class GoogleMTv2Connector extends BaseConnector {
    private static final String BASE_URL = "https://translation.googleapis.com/language/translate/v2";
    private static final int RETRIES = 3;
    private static final int SLEEPPAUSE = 300; // ms

	private GoogleMTv2Parameters params;
	private QueryUtil util;
	private GoogleMTAPI api;

	public GoogleMTv2Connector () {
		params = new GoogleMTv2Parameters();
		util = new QueryUtil();
		api = new GoogleMTAPIImpl(BASE_URL, params);
	}
	
	@Override
	public void setParameters (IParameters params) {
		this.params = (GoogleMTv2Parameters)params;
	}
	
	@Override
	public GoogleMTv2Parameters getParameters () {
		return params;
	}
	
	@Override
	public void close () {
		// Nothing to do
	}

	@Override
	public String getName () {
		return "Google-MTv2";
	}

	@Override
	public String getSettingsDisplay () {
		return "Server: " + BASE_URL;
	}

	@Override
	public void open () {
		// Nothing to do
	}

	@Override
	public int query (String plainText) {
		return _query(plainText, plainText, new TextQueryResultBuilder(params, getName(), getWeight()));
	}
	
	@Override
	public int query (TextFragment frag) {
	    return _query(util.toCodedHTML(frag), frag, new FragmentQueryResultBuilder(params, getName(), getWeight()));
	}

	protected <T> int _query(String queryText, T originalText, QueryResultBuilder<T> resultBuilder) {
	    current = -1;
        if (queryText.isEmpty()) return 0;
        // Check that we have some Key available
        if ( Util.isEmpty(params.getApiKey()) ) {
            throw new OkapiException("You must have a Google API Key to use this connector.");
        }
        List<QueryResult> queryResults = new ArrayList<>();
        try {
            for (int tries = 0; tries < RETRIES; tries++) {
                List<TranslationResponse> responses = api.translate(srcCode, trgCode, Collections.singletonList(queryText));
                if (responses != null) {
                    queryResults = resultBuilder.convertResponses(responses, originalText);
                    break;
                }
                try {
                    Thread.sleep(SLEEPPAUSE);
                } catch (InterruptedException e) {
                    throw new OkapiException("Interrupted while trying to contact Google MT");
                }
            }
        }
        catch ( Throwable e) {
            throw new OkapiException("Error querying the MT server: " + e.getMessage(), e);
        }
        if (queryResults.size() > 0) {
            current = 0;
            result = queryResults.iterator().next();
            return 1;
        }
        throw new OkapiException("Could not retrieve results from Google after " + RETRIES + " attempts.");
	}

    @Override
    public List<List<QueryResult>> batchQueryText(List<String> plainTexts) {
        return _batchQuery(plainTexts, plainTexts, new TextQueryResultBuilder(params, getName(), getWeight()));
    }

    @Override
    public List<List<QueryResult>> batchQuery (List<TextFragment> fragments) {
        return _batchQuery(util.toCodedHTML(fragments), fragments,
                           new FragmentQueryResultBuilder(params, getName(), getWeight()));
    }

    public List<LocaleId> getSupportedLanguages() {
        try {
            for (int tries = 0; tries < RETRIES; tries++) {
                List<String> codes = api.getLanguages();
                if (codes != null) {
                    List<LocaleId> locales = new ArrayList<>();
                    for (String code : codes) {
                        locales.add(convertGoogleLanguageCode(code));
                    }
                    return locales;
                }
                try {
                    Thread.sleep(SLEEPPAUSE);
                } catch (InterruptedException e) {
                    throw new OkapiException("Interrupted while trying to contact Google MT");
                }
            }
        }
        catch ( Throwable e) {
            throw new OkapiException("Error querying the MT server: " + e.getMessage(), e);
        }
        throw new OkapiException("Could not retrieve language list from Google after " + RETRIES + " attempts.");
    }

    protected LocaleId convertGoogleLanguageCode(String lang) {
        return LocaleId.fromBCP47(lang);
    }

    protected <T> List<List<QueryResult>> _batchQuery(List<String> texts, List<T> originalText,
                        QueryResultBuilder<T> qrBuilder) {
        current = -1;
        List<List<QueryResult>> queryResults = new ArrayList<>(texts.size());
        try {
            for (int tries = 0; tries < RETRIES; tries++) {
                List<TranslationResponse> responses = api.translate(srcCode, trgCode, texts);
                if (responses != null) {
                    for (int i = 0; i < responses.size(); i++) {
                        queryResults.add(qrBuilder.convertResponses(
                                Collections.singletonList(responses.get(i)), originalText.get(i)));
                    }
                    break;
                }
                try {
                    Thread.sleep(SLEEPPAUSE);
                } catch (InterruptedException e) {
                    throw new OkapiException("Interrupted while trying to contact Google MT");
                }
            }
        }
        catch ( Throwable e) {
            throw new OkapiException("Error querying the MT server: " + e.getMessage(), e);
        }
        return queryResults;
    }

	@Override
	public void leverage (ITextUnit tu) {
		leverageUsingBatchQuery(tu);
	}

	@Override
	public void batchLeverage (List<ITextUnit> tuList) {
		batchLeverageUsingBatchQuery(tuList);
	}

	@Override
	protected String toInternalCode (LocaleId locale) {
		String code = locale.toBCP47();
		if ( !code.startsWith("zh") && ( code.length() > 2 )) {
			code = code.substring(0, 2);
		}
		return code;
	}	

}
