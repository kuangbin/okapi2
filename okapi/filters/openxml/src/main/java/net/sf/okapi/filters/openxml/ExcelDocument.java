/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Excel;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.ParseType.MSEXCEL;
import static net.sf.okapi.filters.openxml.ParseType.MSEXCELCOMMENT;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDDOCPROPERTIES;

class ExcelDocument extends DocumentType {
	private final Map<String, String> sharedStrings;
	private SharedStringMap sharedStringMap = new SharedStringMap();
	private List<String> worksheetEntryNames = null;
	private ExcelStyles styles;
	private Relationships workbookRels;
	private Map<String, ExcelWorkbook.Sheet> worksheets = new HashMap<>();
	private Map<String, Boolean> tableVisibility = new HashMap<>();
	private static final String SHARED_STRING_TABLE_REL =
			Namespaces.DocumentRelationships.getDerivedURI("/sharedStrings");
	private static final String STYLES_REL =
			Namespaces.DocumentRelationships.getDerivedURI("/styles");

	ExcelDocument(OpenXMLZipFile zipFile, ConditionalParameters params, Map<String, String> sharedStrings) {
		super(zipFile, params);
		this.sharedStrings = sharedStrings;
	}

	@Override
	boolean isClarifiablePart(String contentType) {
		return Excel.STYLES_TYPE.equals(contentType)
				|| Excel.WORKSHEET_TYPE.equals(contentType);
	}

	@Override
	boolean isStyledTextPart(String entryName, String type) {
		return Excel.SHARED_STRINGS_TYPE.equals(type) || Drawing.CHART_TYPE.equals(type);
	}

	@Override
	void initialize() throws IOException, XMLStreamException {
		String mainDocumentPart = getZipFile().getMainDocumentTarget();
		workbookRels = getZipFile().getRelationshipsForTarget(mainDocumentPart);
		worksheetEntryNames = findWorksheets();
		styles = parseStyles();
	}

	@Override
	OpenXMLPartHandler getHandlerForFile(ZipEntry entry, String contentType) {
		if (!isTranslatablePart(entry.getName(), contentType)) {
			if (contentType.equals(Excel.WORKSHEET_TYPE)) {
				// Check to see if it's visible
				return new ExcelWorksheetPartHandler(getZipFile(), entry, sharedStringMap, styles, tableVisibility,
						findWorksheetNumber(entry.getName()), getParams(), isSheetHidden(entry.getName()));
			}
			else if (isClarifiablePart(contentType)) {
				return new ClarifiablePartHandler(getZipFile(), entry);
			}
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}

		if (contentType.equals(Excel.SHARED_STRINGS_TYPE)) {
			return new SharedStringsPartHandler(getParams(), getZipFile(), entry, StyleDefinitions.emptyStyleDefinitions(), sharedStringMap);
		}
		if (contentType.equals(Drawing.CHART_TYPE)) {
			return new StyledTextPartHandler(getParams(), getZipFile(), entry, StyleDefinitions.emptyStyleDefinitions());
		}
		ParseType parseType = null;
		if (contentType.equals(Excel.COMMENT_TYPE)) {
			parseType = ParseType.MSEXCELCOMMENT;
		} else if (Common.CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = MSWORDDOCPROPERTIES;
		} else if (Excel.MAIN_DOCUMENT_TYPE.equals(contentType)) {
			parseType = MSEXCEL;
		}

		if (MSWORDDOCPROPERTIES.equals(parseType) || MSEXCELCOMMENT.equals(parseType) || parseType == MSEXCEL) {
			OpenXMLContentFilter openXMLContentFilter = new OpenXMLContentFilter(getParams());
			openXMLContentFilter.setUpConfig(parseType);
			openXMLContentFilter.setPartName(entry.getName());
			return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry);
		}

		return new ExcelFormulaPartHandler(getParams(), getZipFile(), entry, sharedStrings);
	}

	private boolean isSheetHidden(String entryName) {
		ExcelWorkbook.Sheet sheet = worksheets.get(entryName);

		return sheet != null && !sheet.visible;
	}

	private boolean isTranslatablePart(String entryName, String contentType) {
		if (Excel.TABLE_TYPE.equals(contentType)) {
			Boolean b = tableVisibility.get(entryName);
			// There should always be a value, but default to hiding tables we don't know about
			return (b != null) ? b : false;
		}
		return entryName.endsWith(".xml")
				&& (Excel.SHARED_STRINGS_TYPE.equals(contentType)
					|| Drawing.CHART_TYPE.equals(contentType)
				    || getParams().getTranslateExcelSheetNames() && (Excel.MAIN_DOCUMENT_TYPE.equals(contentType) || Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE.equals(contentType))
					|| getParams().getTranslateDocProperties() && Common.CORE_PROPERTIES_TYPE.equals(contentType)
					|| getParams().getTranslateComments() && Excel.COMMENT_TYPE.equals(contentType));
	}

	/**
	 * Do additional reordering of the entries for XLSX files to make
	 * sure that worksheets are parsed in order, followed by the shared
	 * strings table.
	 * @return the sorted enum of ZipEntry
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	@Override
	Enumeration<? extends ZipEntry> getZipFileEntries() throws IOException, XMLStreamException {
		Enumeration<? extends ZipEntry> entries = getZipFile().entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		List<String> worksheetsAndSharedStrings = new ArrayList<String>();
		worksheetsAndSharedStrings.addAll(worksheetEntryNames);
		worksheetsAndSharedStrings.add(findSharedStrings());
		Collections.sort(entryList, new ZipEntryComparator(worksheetsAndSharedStrings));
		return Collections.enumeration(entryList);
	}

	ExcelWorkbook parseWorkbook(String partName) throws IOException, XMLStreamException {
		XMLEventReader r = getZipFile().getInputFactory().createXMLEventReader(getZipFile().getPartReader(partName));
		return ExcelWorkbook.parseFrom(r, getParams());
	}

	ExcelStyles parseStyles() throws IOException, XMLStreamException {
		Relationships.Rel stylesRel = workbookRels.getRelByType(STYLES_REL).get(0);
		ExcelStyles styles = new ExcelStyles();
		styles.parse(getZipFile().getInputFactory().createXMLEventReader(
					 getZipFile().getPartReader(stylesRel.target)));
		return styles;
	}

	/**
	 * Examine relationship information to find all worksheets in the package.
	 * Return a list of their entry names, in order.
	 * @return list of entry names.
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	List<String> findWorksheets() throws IOException, XMLStreamException {
		List<String> worksheetNames = new ArrayList<String>();
		ExcelWorkbook workbook = parseWorkbook(getZipFile().getMainDocumentTarget());

		List<ExcelWorkbook.Sheet> sheets = workbook.getSheets();
		for (ExcelWorkbook.Sheet sheet : sheets) {
			Relationships.Rel sheetRel = workbookRels.getRelById(sheet.relId);
			worksheetNames.add(sheetRel.target);
			worksheets.put(sheetRel.target, sheet);
		}
		return worksheetNames;
	}

	/**
	 * Parse relationship information to find the shared strings table.  Return
	 * its entry name.
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	String findSharedStrings() throws IOException, XMLStreamException {
		String mainDocumentPart = getZipFile().getMainDocumentTarget();
		Relationships rels = getZipFile().getRelationshipsForTarget(mainDocumentPart);
		List<Relationships.Rel> r = rels.getRelByType(SHARED_STRING_TABLE_REL);
		if (r == null || r.size() != 1) {
			throw new OkapiBadFilterInputException(UNEXPECTED_NUMBER_OF_RELATIONSHIPS);
		}
		return r.get(0).target;
	}

	private int findWorksheetNumber(String worksheetEntryName) {
		for (int i = 0; i < worksheetEntryNames.size(); i++) {
			if (worksheetEntryName.equals(worksheetEntryNames.get(i))) {
				return i + 1; // 1-indexed
			}
		}
		throw new IllegalStateException("No worksheet entry with name " +
						worksheetEntryName + " in " + worksheetEntryNames);
	}
}
